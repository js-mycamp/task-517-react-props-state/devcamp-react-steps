import { Component } from "react";

class Steps extends Component{
    render(){
        return(
            <>
            {this.props.id} . {this.props.title} : {this.props.content} 
            </>
        )
    }
}

export default Steps
